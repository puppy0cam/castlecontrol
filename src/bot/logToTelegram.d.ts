import { InfoMessage, LOG_LEVELS, LOG_LEVEL_NAMES, InfoMessageConstructor } from "./StackBuilder";
export declare function getLogLevel(level: LOG_LEVEL_NAMES | LOG_LEVELS): LOG_LEVELS;
export declare function getLogLevelConstructor(level: LOG_LEVELS): InfoMessageConstructor;
export default function logToTelegram(type: LOG_LEVELS | LOG_LEVEL_NAMES, ...args: Parameters<typeof Error>): void;
export default function logToTelegram(data: InfoMessage): void;
//# sourceMappingURL=logToTelegram.d.ts.map