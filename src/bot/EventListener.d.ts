import { Update } from "../telegram/types/Update";
import { possible_event_results } from "./EventFilters";
export declare type eventTypes = string | RegExp;
export declare type modifierThing<T> = (data: Update) => possible_event_results | Promise<possible_event_results>;
export declare type callbackType<T> = T extends import("../telegram/types/Update").Update ? import("../telegram/types/Update").Update : T extends import("../telegram/types/Message").Message ? import("../telegram/types/Message").Message : T extends import("../telegram/types/CallbackQuery").CallbackQuery ? import("../telegram/types/CallbackQuery").CallbackQuery : T extends import("../telegram/types/ChosenInlineResult").ChosenInlineResult ? import("../telegram/types/ChosenInlineResult").ChosenInlineResult : T extends import("../telegram/types/InlineQuery").InlineQuery ? import("../telegram/types/InlineQuery").InlineQuery : T extends import("../telegram/types/PreCheckoutQuery").PreCheckoutQuery ? import("../telegram/types/PreCheckoutQuery").PreCheckoutQuery : T extends import("../telegram/types/ShippingQuery").ShippingQuery ? import("../telegram/types/ShippingQuery").ShippingQuery : never;
export declare type callbackTypeConstructor<T> = T extends import("../telegram/types/Update").Update ? typeof import("../telegram/types/Update").Update : T extends import("../telegram/types/Message").Message ? typeof import("../telegram/types/Message").Message : T extends import("../telegram/types/CallbackQuery").CallbackQuery ? typeof import("../telegram/types/CallbackQuery").CallbackQuery : T extends import("../telegram/types/ChosenInlineResult").ChosenInlineResult ? typeof import("../telegram/types/ChosenInlineResult").ChosenInlineResult : T extends import("../telegram/types/InlineQuery").InlineQuery ? typeof import("../telegram/types/InlineQuery").InlineQuery : T extends import("../telegram/types/PreCheckoutQuery").PreCheckoutQuery ? typeof import("../telegram/types/PreCheckoutQuery").PreCheckoutQuery : T extends import("../telegram/types/ShippingQuery").ShippingQuery ? typeof import("../telegram/types/ShippingQuery").ShippingQuery : never;
declare type IEventListener<T = any> = {
    [P in keyof T]-?: T[P];
};
interface OptionalEventListenerParts<T extends import("../types/bot/TypeSwappers").CallbackConstructorTypes> {
    listeners?: eventTypes[];
    remainingCalls?: number;
    modifiers?: Array<modifierThing<T["prototype"]>>;
}
interface RequiredEventListenerParts<T extends import("../types/bot/TypeSwappers").CallbackConstructorTypes> {
    argumentConstructor: T;
    callback(data: callbackType<T["prototype"]>): void | Promise<void>;
}
interface ConstructingEventListener<T extends import("../types/bot/TypeSwappers").CallbackConstructorTypes> extends OptionalEventListenerParts<T>, RequiredEventListenerParts<T> {
}
/**
 * represents an event that may be called upon.
 * Includes functionality that will convert the update to the requested type.
 */
export default class EventListener<T extends import("../types/bot/TypeSwappers").CallbackConstructorTypes = any> implements IEventListener {
    argumentConstructor: T;
    listeners: eventTypes[];
    remainingCalls: number;
    modifiers: Array<modifierThing<Update>>;
    constructor({ argumentConstructor, callback, listeners, modifiers, remainingCalls, }: ConstructingEventListener<T>);
    /**
     * While this is initially a blank function to minimise errors,
     * it is assigned to on object creation.
     * @param data the data to be passed
     */
    callback(data: callbackType<T["prototype"]>): void | Promise<void>;
    getRequiredDataFromUpdate(update: Update): T["prototype"] | void;
    execute(update: Update): Promise<void>;
    doesUpdateMeetBasicRequirements(update: Update): Promise<boolean>;
}
export {};
//# sourceMappingURL=EventListener.d.ts.map