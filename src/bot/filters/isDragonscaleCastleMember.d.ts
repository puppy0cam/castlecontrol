import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function isDragonscaleCastleMember(data?: Update | Message): Promise<import("../EventFilters").possible_event_results>;
//# sourceMappingURL=isDragonscaleCastleMember.d.ts.map