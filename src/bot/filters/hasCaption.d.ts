import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function hasCaption(data?: Update): import("../EventFilters").possible_event_results;
export default function hasCaption(data?: Message): import("../EventFilters").possible_event_results;
//# sourceMappingURL=hasCaption.d.ts.map