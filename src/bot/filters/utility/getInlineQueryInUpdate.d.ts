import { Update } from "../../../telegram/types/Update";
import { InlineQuery } from "../../../telegram/types/InlineQuery";
export default function getInlineQueryInUpdate(update: Update): InlineQuery | undefined;
//# sourceMappingURL=getInlineQueryInUpdate.d.ts.map