import { Update } from "../../../telegram/types/Update";
import { ChosenInlineResult } from "../../../telegram/types/ChosenInlineResult";
export default function getChosenInlineResultInUpdate(update: Update): ChosenInlineResult | undefined;
//# sourceMappingURL=getChosenInlineResultInUpdate.d.ts.map