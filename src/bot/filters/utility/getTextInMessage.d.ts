import { Update } from "../../../telegram/types/Update";
import { MessageEntity } from "../../../telegram/types/MessageEntity";
import { Message } from "../../../telegram/types/Message";
interface MessageString {
    text: string;
    entities: MessageEntity[];
    type: "caption" | "text" | null;
}
export default function getTextInMessage(data?: Update): MessageString;
export default function getTextInMessage(data?: Message): MessageString;
export default function getTextInMessage(data?: Update | Message): MessageString;
export {};
//# sourceMappingURL=getTextInMessage.d.ts.map