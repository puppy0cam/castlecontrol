import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function hasText(data?: Update): import("../EventFilters").possible_event_results;
export default function hasText(data?: Message): import("../EventFilters").possible_event_results;
//# sourceMappingURL=hasText.d.ts.map