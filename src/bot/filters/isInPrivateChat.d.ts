import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function isInPrivateChat(data?: Update): import("../EventFilters").possible_event_results;
export default function isInPrivateChat(data?: Message): import("../EventFilters").possible_event_results;
//# sourceMappingURL=isInPrivateChat.d.ts.map