import { CallbackQuery } from "../../telegram/types/CallbackQuery";
import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function isCommander(data?: Update): import("../EventFilters").possible_event_results;
export default function isCommander(data?: Message): import("../EventFilters").possible_event_results;
export default function isCommander(data?: CallbackQuery): import("../EventFilters").possible_event_results;
//# sourceMappingURL=isCommander.d.ts.map