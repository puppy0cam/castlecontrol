declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const verbose: unique symbol;
export declare type verbose = typeof verbose;
export default class VerboseMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): VerboseMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=VerboseMessage.d.ts.map