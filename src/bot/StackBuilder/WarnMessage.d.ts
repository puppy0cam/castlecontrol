declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const warn: unique symbol;
export declare type warn = typeof warn;
export default class WarnMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): WarnMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=WarnMessage.d.ts.map