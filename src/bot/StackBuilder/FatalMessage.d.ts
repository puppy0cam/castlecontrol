declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const fatal: unique symbol;
export declare type fatal = typeof fatal;
export default class FatalMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): FatalMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=FatalMessage.d.ts.map