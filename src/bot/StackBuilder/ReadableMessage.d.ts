declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const readable: unique symbol;
export declare type readable = typeof readable;
export default class ReadableMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): ReadableMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=ReadableMessage.d.ts.map