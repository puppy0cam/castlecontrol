declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const log: unique symbol;
export declare type log = typeof log;
export default class LogMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): LogMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=LogMessage.d.ts.map