/// <reference types="node" />
interface ValueFixer<Value = any> {
    (): ValueFixer<Value>;
    [key: string]: ValueFixer<Value>;
}
interface NodeModule {
    tree: ValueFixer<typeof module & typeof import("module").prototype>;
    getModuleLocation(): string;
    getTopLevel(): this;
    loadScript(): void;
    scriptLoaded(): void;
    requireOld(id: string): any;
    /**
     * This will create a function that will do the steps for require only when it is actually needed.
     * This can help boost performance with minimal code changes.
     * @param path the path to the module
     */
    setupNeededValue<T>(path: string): () => T;
}
//# sourceMappingURL=Module.d.ts.map