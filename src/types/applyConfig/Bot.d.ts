declare type Basic = import("./Basic").default<"TelegramBot">;
export default interface Bot extends Basic {
    token: string;
    shouldTick?: boolean;
}
export {};
//# sourceMappingURL=Bot.d.ts.map