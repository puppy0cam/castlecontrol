declare type Basic = import("./Basic").default<"process">;
export default interface Process extends Basic {
    title?: string;
}
export {};
//# sourceMappingURL=Process.d.ts.map