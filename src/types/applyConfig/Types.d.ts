declare type MySQL = import("./MySQL").MySQL;
declare type MongoDB = import("./MongoDB").MongoDB;
declare type AMQP = import("./AMQP").default;
declare type Process = import("./Process").default;
declare type Bot = import("./Bot").default;
export declare type Types = MySQL | AMQP | Process | Bot | MongoDB;
export {};
//# sourceMappingURL=Types.d.ts.map