declare type BasicDatabase = import("./BasicDatabase").default;
export default interface Local extends BasicDatabase {
    /** This is probably going to be `/var/lib/mysql/mysql.sock` */
    socketPath: string;
}
export {};
//# sourceMappingURL=Local.d.ts.map