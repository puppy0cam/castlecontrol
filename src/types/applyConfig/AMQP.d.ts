declare type Connect = import("amqplib").Options.Connect;
declare type Basic = import("./Basic").default<"amqp">;
export default interface AMQP extends Basic, Connect {
}
export {};
//# sourceMappingURL=AMQP.d.ts.map