declare type Basic = import("./Basic").default<"MongoDb">;
export interface MongoDB extends Basic {
    db?: string;
    host: string;
    /** `27083` is probably what you are looking for, as the port is not the default for security purposes. */
    port: number;
    options?: import("mongodb").MongoClientOptions;
}
export {};
//# sourceMappingURL=MongoDB.d.ts.map