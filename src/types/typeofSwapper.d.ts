/*
This is a utility that you can use if you want to create a type for swapping two types.
*/
export declare type typeofResult = "boolean" | "function" | "number" | "object" | "string" | "symbol" | "undefined";
export declare type typeofRefers = boolean | Function | number | object | string | symbol | undefined;
declare type Diff<T, U> = import("./Diff").Diff<T, U>;
export declare type booleanSwapper<T> = T extends boolean ? "boolean" : T extends "boolean" ? boolean : never;
export declare type functionSwapper<T> = T extends Function ? "function" : T extends "function" ? Function : never;
export declare type numberSwapper<T> = T extends number ? "number" : T extends "number" ? number : never;
export declare type objectSwapper<T> = T extends object ? "object" : T extends "object" ? object : never;
export declare type stringSwapper<T> = T extends "string" ? string : T extends Diff<"string", typeofResult> ? string : never;
export declare type symbolSwapper<T> = T extends symbol ? "symbol" : T extends "symbol" ? number : never;
export declare type undefinedSwapper<T> = T extends undefined ? "undefined" : T extends "undefined" ? undefined : never;
export declare type typeofSwapper<T extends typeofResult | typeofRefers> = T extends "boolean" ? boolean : T extends "function" ? Function : T extends "number" ? number : T extends "object" ? object : T extends "string" ? string : T extends "symbol" ? symbol : T extends "undefined" ? undefined : T extends boolean ? "boolean" : T extends Function ? "function" : T extends number ? "number" : T extends object ? "object" : T extends symbol ? "symbol" : T extends undefined ? "undefined" : T extends string ? T extends typeofResult ? never : "string" : never;
export declare type Swapper2<T, A, B, Fallback = never, ReturnTo = A> = T extends A ? B : T extends B ? ReturnTo : Fallback;
export declare type Swapper3<T, A, B, C, Fallback = never, ReturnTo = A> = Swapper2<T, A, B, T extends C ? ReturnTo : Fallback, C>;
export declare type Swapper4<T, A, B, C, D, Fallback = never, ReturnTo = A> = Swapper3<T, A, B, C, T extends D ? ReturnTo : Fallback, D>;
export declare type Swapper5<T, A, B, C, D, E, Fallback = never, ReturnTo = A> = Swapper4<T, A, B, C, D, T extends E ? ReturnTo : Fallback, E>;
export declare type Swapper6<T, A, B, C, D, E, F, Fallback = never, ReturnTo = A> = Swapper5<T, A, B, C, D, E, T extends F ? ReturnTo : Fallback, F>;
export declare type Swapper7<T, A, B, C, D, E, F, G, Fallback = never, ReturnTo = A> = Swapper6<T, A, B, C, D, E, F, T extends G ? ReturnTo : Fallback, G>;
export declare type Swapper8<T, A, B, C, D, E, F, G, H, Fallback = never, ReturnTo = A> = Swapper7<T, A, B, C, D, E, F, G, T extends H ? ReturnTo : Fallback, H>;
export declare type Swapper9<T, A, B, C, D, E, F, G, H, I, Fallback = never, ReturnTo = A> = Swapper8<T, A, B, C, D, E, F, G, H, T extends I ? ReturnTo : Fallback, I>;
export declare type Swapper10<T, A, B, C, D, E, F, G, H, I, J, Fallback = never, ReturnTo = A> = Swapper9<T, A, B, C, D, E, F, G, H, I, T extends J ? ReturnTo : Fallback, J>;
export declare type Swapper11<T, A, B, C, D, E, F, G, H, I, J, K, Fallback = never, ReturnTo = A> = Swapper10<T, A, B, C, D, E, F, G, H, I, J, T extends K ? ReturnTo : Fallback, K>;
export {};
//# sourceMappingURL=typeofSwapper.d.ts.map