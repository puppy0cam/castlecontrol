interface AsyncIterableIterator<T> {
    next(value?: any): Promise<IteratorResult<T>>;
    return?(value?: any): Promise<IteratorResult<T>>;
    throw?(e?: any): Promise<IteratorResult<T>>;
}
//# sourceMappingURL=AsyncIterableIterator.d.ts.map