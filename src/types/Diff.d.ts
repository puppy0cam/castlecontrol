export declare type Diff<T, U> = T extends U ? never : T;
//# sourceMappingURL=Diff.d.ts.map