export declare const PROCESS_NAME: string;
export declare const BOT_TOKENS: {
    DOGE_GENERAL: string;
    KANA: string;
    UTILITY: string;
};
export declare const MongoDBConfig: import("./types/applyConfig/MongoDB").MongoDB;
export declare const ExternalAMQP: import("./types/applyConfig/AMQP").default;
export declare const InternalAMQP: import("./types/applyConfig/AMQP").default;
export declare const LOGGING_CHANNEL: number;
export declare const GAME_BOT_ID: number;
//# sourceMappingURL=config.d.ts.map