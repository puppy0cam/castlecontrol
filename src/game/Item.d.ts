export interface ItemData {
    readonly itemName: string;
    readonly itemCode?: string;
    use: () => string;
    sell: () => string;
    buy: () => string;
    getpet: () => string;
    on: () => string;
    off: () => string;
    view: () => string;
    bind: () => string;
    wrap: () => string;
    lot: () => string;
    t: () => string;
    a: (amount?: number) => string;
    g_deposit: (amount?: number) => string;
    g_withdraw: (amount?: number) => string;
    craft: (amount?: number) => string;
    c: (amount?: number) => string;
    wtb: (amount?: number) => string;
    wts: (amount?: number, price?: number) => string;
    brew: (amount?: number) => string;
    b: (amount?: number) => string;
    d: (amount?: number) => string;
    da: (amount?: number) => string;
    aa: (amount?: number) => string;
}
export default class Item implements ItemData {
    readonly itemName: string;
    readonly itemCode?: string | undefined;
    static itemList: Item[];
    static getItemFromCode(item: string): Item | null;
    constructor(itemName: string, itemCode?: string | undefined);
    use(): string;
    sell(): string;
    buy(): string;
    getpet(): string;
    on(): string;
    off(): string;
    view(): string;
    bind(): string;
    wrap(): string;
    lot(): string;
    t(): string;
    a(amount?: number): string;
    g_deposit(amount?: number): string;
    g_withdraw(amount?: number): string;
    craft(amount?: number): string;
    c(amount?: number): string;
    wtb(amount?: number): string;
    wts(amount?: number, price?: number): string;
    brew(amount?: number): string;
    b(amount?: number): string;
    d(amount?: number): string;
    da(amount?: number): string;
    aa(amount?: number): string;
}
//# sourceMappingURL=Item.d.ts.map