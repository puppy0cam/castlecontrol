import Item from "./Item";
export interface RecipeData {
    items: Map<Item, number>;
    result: Item;
    gold?: number;
    mana?: number;
}
export declare type costType = "mana" | "gold";
export declare type Cost = {
    [key in costType]?: number;
};
export default class Recipe implements RecipeData {
    readonly result: Item;
    gold?: number;
    mana?: number;
    items: Map<Item, number>;
    constructor(result: Item, cost?: Cost);
}
//# sourceMappingURL=Recipe.d.ts.map