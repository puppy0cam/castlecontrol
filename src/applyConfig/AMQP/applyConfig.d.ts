export default function applyConfig(config: import("../../types/applyConfig/AMQP").default): Promise<AMQPSetup>;
declare class AMQPSetup {
    private _config;
    connection: import("amqplib").Connection | undefined;
    channel: import("amqplib").ConfirmChannel | undefined;
    tasksEveryConnection: Array<(amqp: AMQPSetup) => void>;
    private _connect;
    private _connecting;
    private _isConnected;
    constructor(_config: import("../../types/applyConfig/AMQP").default);
    ensureConnection(): Promise<void>;
    private _connector;
}
export {};
//# sourceMappingURL=applyConfig.d.ts.map