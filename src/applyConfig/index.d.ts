/**
 * Creates a connection to the RabbitMQ server.
 * @param config the config for the connection. Once applied, you must apply a new config to change the config.
 */
export default function applyConfig(config: import("../types/applyConfig/AMQP").default): ReturnType<typeof import("./AMQP/applyConfig").default>;
/**
 * Creates a knex config to connect to the MySQL database.
 * @param config the config used for connection to the database
 */
export default function applyConfig(config: import("../types/applyConfig/MySQL").MySQL): ReturnType<typeof import("./MySQL/applyConfig").default>;
/**
 * Creates a config to connect to the MongoDB database.
 * @param config the config used for connection to the database
 */
export default function applyConfig(config: import("../types/applyConfig/MongoDB").MongoDB): ReturnType<typeof import("./MongoDB/applyConfig").default>;
/**
 * sets various values in the process.
 * @param config The config to be used when setting the process data
 */
export default function applyConfig(config: import("../types/applyConfig/Process").default): ReturnType<typeof import("./Process/applyConfig").default>;
/**
 * sets up a bot to be used to interact with telegram
 * @param config the config to be used when preparing the bot
 */
export default function applyConfig(config: import("../types/applyConfig/Bot").default): ReturnType<typeof import("./Bot/applyConfig").default>;
//# sourceMappingURL=index.d.ts.map