import { ChatWars } from "../../AMQP/chatwars";
import TelegramBot from "../../bot/TelegramBot";
export default function setupCreateAuthCode(): Promise<void>;
export declare function getCreateAuthCodeListener(bot: TelegramBot): (data: ChatWars.API.Response.createAuthCode) => Promise<void>;
//# sourceMappingURL=setupCreateAuthCode.d.ts.map