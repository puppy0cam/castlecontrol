import TelegramBot from "../../bot/TelegramBot";
import { Message } from "../../telegram/types/Message";
export default function setupRevokeCommand(): Promise<void>;
export declare function getRevokeListener(bot: TelegramBot): (msg: Message) => Promise<void>;
//# sourceMappingURL=setupRevokeCommand.d.ts.map