/**
 * Retrieves a mongoDB connection.
 * If there is not connection and there is not one being ob
 */
export default function getMongoConnection(): Promise<import("mongodb").MongoClient>;
//# sourceMappingURL=MongoConnection.d.ts.map