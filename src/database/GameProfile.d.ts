import { MetadataForInterface } from "./DatabaseTemplate";
import { ChatWars } from "../AMQP/chatwars";
export declare function parseRequestProfilePayloadProfile(data: ChatWars.API.Response.requestProfile.Ok["payload"]["profile"]): MetadataForInterface<typeof data>;
export declare function parseRequestProfilePayload(data: ChatWars.API.Response.requestProfile.Ok["payload"]): MetadataForInterface<typeof data>;
export declare function parseRequestProfile(data: ChatWars.API.Response.requestProfile.Ok): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: ChatWars.API.Response.requestProfile.Ok | MetadataForInterface<ChatWars.API.Response.requestProfile.Ok>): {
        getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.requestProfile.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.requestProfile.Ok | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.requestProfile.Ok | null>;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.requestProfile.Ok | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
        toSimpleObject(): Partial<ChatWars.API.Response.requestProfile.Ok>;
    };
    getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.requestProfile.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, update: ChatWars.API.Response.requestProfile.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.requestProfile.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.requestProfile.Ok | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.requestProfile.Ok | null>;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.requestProfile.Ok | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.requestProfile.Ok>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.requestProfile.Ok>>): void;
};
export default class GameProfile extends BaseClass implements ChatWars.API.Response.requestProfile.Ok {
    action: "requestProfile";
    payload: {
        profile: {
            atk?: number;
            castle?: string;
            class?: string;
            def?: number;
            exp?: number;
            gold?: number;
            guild?: string;
            guild_tag?: string;
            lvl?: number;
            mana?: number;
            pouches?: number;
            stamina?: number;
            userName?: string;
        };
        userId: number;
    };
    result: "Ok";
}
export {};
//# sourceMappingURL=GameProfile.d.ts.map