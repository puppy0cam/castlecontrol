declare type DynamicValues = import("../util/InterfaceParameters").DynamicValues;
import { FindOneOptions, ClientSession, FindOneAndUpdateOption, MongoCallback, FilterQuery, FindAndModifyWriteOpResultObject, UpdateQuery } from "mongodb";
export declare type MetadataForInterface<T extends {
    [key: string]: any;
}> = T & {
    _refreshDate: Date;
};
/**
 * this function will create a class for data to be processed
 */
export default function createDataClass<T extends DynamicValues>(name: string, parser: (data: T | MetadataForInterface<T>) => MetadataForInterface<T>, ...identifyingData: Array<Array<keyof T>>): {
    new (data: T | MetadataForInterface<T>): {
        getCollection(): Promise<import("mongodb").Collection<T>>;
        _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
        _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, options?: FindOneAndUpdateOption | undefined): Promise<FindAndModifyWriteOpResultObject<T>>;
        _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, options: FindOneAndUpdateOption, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
        _findOne(filter: FilterQuery<T>, callback: MongoCallback<T | null>): void;
        _findOne(filter: FilterQuery<T>, options?: FindOneOptions | undefined): Promise<T | null>;
        _findOne(filter: FilterQuery<T>, options: FindOneOptions, callback: MongoCallback<T | null>): void;
        _findOneAndDelete(filter: FilterQuery<T>, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
        _findOneAndDelete(filter: FilterQuery<T>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: ClientSession | undefined;
        } | undefined): Promise<FindAndModifyWriteOpResultObject<T>>;
        _findOneAndDelete(filter: FilterQuery<T>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: ClientSession | undefined;
        }, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
        toSimpleObject(): Partial<T>;
    };
    getCollection(): Promise<import("mongodb").Collection<T>>;
    _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
    _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, options?: FindOneAndUpdateOption | undefined): Promise<FindAndModifyWriteOpResultObject<T>>;
    _findOneAndUpdate(filter: FilterQuery<T>, update: T | UpdateQuery<T>, options: FindOneAndUpdateOption, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
    _findOne(filter: FilterQuery<T>, callback: MongoCallback<T | null>): void;
    _findOne(filter: FilterQuery<T>, options?: FindOneOptions | undefined): Promise<T | null>;
    _findOne(filter: FilterQuery<T>, options: FindOneOptions, callback: MongoCallback<T | null>): void;
    _findOneAndDelete(filter: FilterQuery<T>, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
    _findOneAndDelete(filter: FilterQuery<T>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: ClientSession | undefined;
    } | undefined): Promise<FindAndModifyWriteOpResultObject<T>>;
    _findOneAndDelete(filter: FilterQuery<T>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: ClientSession | undefined;
    }, callback: MongoCallback<FindAndModifyWriteOpResultObject<T>>): void;
};
export {};
//# sourceMappingURL=DatabaseTemplate.d.ts.map