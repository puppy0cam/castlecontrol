import { MetadataForInterface } from "./DatabaseTemplate";
export interface IAuction {
    lotId: string | number;
    itemName: string;
    sellerName: string;
    quality?: string;
    sellerCastle: string;
    endAt: string | Date;
    startedAt: string | Date;
    buyerCastle?: string;
    price: number;
}
export declare type AuctionDigest = Array<IAuction | Auction>;
export declare function parseAuction(data: IAuction): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: IAuction | MetadataForInterface<IAuction>): {
        getCollection(): Promise<import("mongodb").Collection<IAuction>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
        _findOne(filter: import("mongodb").FilterQuery<IAuction>, callback: import("mongodb").MongoCallback<IAuction | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<IAuction>, options?: import("mongodb").FindOneOptions | undefined): Promise<IAuction | null>;
        _findOne(filter: import("mongodb").FilterQuery<IAuction>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IAuction | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
        toSimpleObject(): Partial<IAuction>;
    };
    getCollection(): Promise<import("mongodb").Collection<IAuction>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IAuction>, update: IAuction | import("mongodb").UpdateQuery<IAuction>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
    _findOne(filter: import("mongodb").FilterQuery<IAuction>, callback: import("mongodb").MongoCallback<IAuction | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<IAuction>, options?: import("mongodb").FindOneOptions | undefined): Promise<IAuction | null>;
    _findOne(filter: import("mongodb").FilterQuery<IAuction>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IAuction | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IAuction>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IAuction>>): void;
};
export default class Auction extends BaseClass implements IAuction {
    buyerCastle?: string;
    endAt: string | Date;
    itemName: string;
    lotId: string | number;
    price: number;
    quality?: string;
    sellerCastle: string;
    sellerName: string;
    startedAt: string | Date;
}
export {};
//# sourceMappingURL=Auction.d.ts.map