export default function collectionBuilder<T>(name: string): () => Promise<import("mongodb").Collection<T>>;
//# sourceMappingURL=MongoCollection.d.ts.map