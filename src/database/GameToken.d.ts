import { MetadataForInterface } from "./DatabaseTemplate";
import { ChatWars } from "../AMQP/chatwars";
export declare function parseGrantTokenPayload(data: ChatWars.API.Response.grantToken.Ok["payload"]): MetadataForInterface<typeof data>;
export declare function parseGrantToken(data: ChatWars.API.Response.grantToken.Ok): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: ChatWars.API.Response.grantToken.Ok | MetadataForInterface<ChatWars.API.Response.grantToken.Ok>): {
        getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.grantToken.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.grantToken.Ok | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.grantToken.Ok | null>;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.grantToken.Ok | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
        toSimpleObject(): Partial<ChatWars.API.Response.grantToken.Ok>;
    };
    getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.grantToken.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, update: ChatWars.API.Response.grantToken.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.grantToken.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.grantToken.Ok | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.grantToken.Ok | null>;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.grantToken.Ok | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.grantToken.Ok>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.grantToken.Ok>>): void;
};
export default class GameToken extends BaseClass implements ChatWars.API.Response.grantToken.Ok {
    action: "grantToken";
    payload: {
        userId: number;
        id: string;
        token: string;
    };
    result: "Ok";
}
export {};
//# sourceMappingURL=GameToken.d.ts.map