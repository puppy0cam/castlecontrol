import { Update } from "../telegram/types/Update";
import { MetadataForInterface } from "./DatabaseTemplate";
interface ITelegramUpdateLog extends Update {
}
export declare function parseUpdate(data: ITelegramUpdateLog): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: ITelegramUpdateLog | MetadataForInterface<ITelegramUpdateLog>): {
        getCollection(): Promise<import("mongodb").Collection<ITelegramUpdateLog>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
        _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<ITelegramUpdateLog | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options?: import("mongodb").FindOneOptions | undefined): Promise<ITelegramUpdateLog | null>;
        _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ITelegramUpdateLog | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
        toSimpleObject(): Partial<ITelegramUpdateLog>;
    };
    getCollection(): Promise<import("mongodb").Collection<ITelegramUpdateLog>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, update: ITelegramUpdateLog | import("mongodb").UpdateQuery<ITelegramUpdateLog>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
    _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<ITelegramUpdateLog | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options?: import("mongodb").FindOneOptions | undefined): Promise<ITelegramUpdateLog | null>;
    _findOne(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ITelegramUpdateLog | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ITelegramUpdateLog>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ITelegramUpdateLog>>): void;
};
export default class TelegramUpdateLog extends BaseClass implements ITelegramUpdateLog {
    update_id: number;
    message?: import("../telegram/types/Message").Message | undefined;
    edited_message?: import("../telegram/types/Message").Message | undefined;
    channel_post?: import("../telegram/types/Message").Message | undefined;
    edited_channel_post?: import("../telegram/types/Message").Message | undefined;
    inline_query?: import("../telegram/types/InlineQuery").InlineQuery | undefined;
    chosen_inline_result?: import("../telegram/types/ChosenInlineResult").ChosenInlineResult | undefined;
    callback_query?: import("../telegram/types/CallbackQuery").CallbackQuery | undefined;
    shipping_query?: import("../telegram/types/ShippingQuery").ShippingQuery | undefined;
    pre_checkout_query?: import("../telegram/types/PreCheckoutQuery").PreCheckoutQuery | undefined;
}
export {};
//# sourceMappingURL=TelegramUpdateLog.d.ts.map