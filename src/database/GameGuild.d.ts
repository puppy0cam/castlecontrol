import { MetadataForInterface } from "./DatabaseTemplate";
import { ChatWars } from "../AMQP/chatwars";
export declare function parseGuildInfoPayloadStock(data: ChatWars.API.Response.guildInfo.Ok["payload"]["stock"]): MetadataForInterface<typeof data>;
export declare function parseGuildInfoPayload(data: ChatWars.API.Response.guildInfo.Ok["payload"]): MetadataForInterface<typeof data>;
export declare function parseGuildInfo(data: ChatWars.API.Response.guildInfo.Ok): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: ChatWars.API.Response.guildInfo.Ok | MetadataForInterface<ChatWars.API.Response.guildInfo.Ok>): {
        getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.guildInfo.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.guildInfo.Ok | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.guildInfo.Ok | null>;
        _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.guildInfo.Ok | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
        toSimpleObject(): Partial<ChatWars.API.Response.guildInfo.Ok>;
    };
    getCollection(): Promise<import("mongodb").Collection<ChatWars.API.Response.guildInfo.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, update: ChatWars.API.Response.guildInfo.Ok | import("mongodb").UpdateQuery<ChatWars.API.Response.guildInfo.Ok>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<ChatWars.API.Response.guildInfo.Ok | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options?: import("mongodb").FindOneOptions | undefined): Promise<ChatWars.API.Response.guildInfo.Ok | null>;
    _findOne(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<ChatWars.API.Response.guildInfo.Ok | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<ChatWars.API.Response.guildInfo.Ok>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<ChatWars.API.Response.guildInfo.Ok>>): void;
};
export default class GameGuild extends BaseClass implements ChatWars.API.Response.guildInfo.Ok {
    action: "guildInfo";
    payload: {
        tag: string;
        level: number;
        castle: string;
        glory: number;
        members: number;
        name: string;
        lobby: string;
        stock: {
            [item: string]: number;
        };
        userId: number;
    };
    result: "Ok";
}
export {};
//# sourceMappingURL=GameGuild.d.ts.map