import { MetadataForInterface } from "./DatabaseTemplate";
export interface IDeal {
    sellerId: string;
    sellerCastle: string;
    sellerName: string;
    buyerId: string;
    buyerCastle: string;
    buyerName: string;
    item: string;
    price: number;
    qty: number;
}
export declare function parseDeal(data: IDeal): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: IDeal | MetadataForInterface<IDeal>): {
        getCollection(): Promise<import("mongodb").Collection<IDeal>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
        _findOne(filter: import("mongodb").FilterQuery<IDeal>, callback: import("mongodb").MongoCallback<IDeal | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<IDeal>, options?: import("mongodb").FindOneOptions | undefined): Promise<IDeal | null>;
        _findOne(filter: import("mongodb").FilterQuery<IDeal>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IDeal | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
        toSimpleObject(): Partial<IDeal>;
    };
    getCollection(): Promise<import("mongodb").Collection<IDeal>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IDeal>, update: IDeal | import("mongodb").UpdateQuery<IDeal>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
    _findOne(filter: import("mongodb").FilterQuery<IDeal>, callback: import("mongodb").MongoCallback<IDeal | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<IDeal>, options?: import("mongodb").FindOneOptions | undefined): Promise<IDeal | null>;
    _findOne(filter: import("mongodb").FilterQuery<IDeal>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IDeal | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IDeal>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IDeal>>): void;
};
export default class Deal extends BaseClass implements IDeal {
    buyerCastle: string;
    buyerId: string;
    buyerName: string;
    sellerId: string;
    sellerCastle: string;
    sellerName: string;
    item: string;
    price: number;
    qty: number;
}
export {};
//# sourceMappingURL=Deal.d.ts.map