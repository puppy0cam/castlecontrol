declare type MongoClientCommonOption = import("mongodb").MongoClientCommonOption;
interface DatabaseOption {
    databaseName?: string;
    options?: MongoClientCommonOption;
}
export default function getMongoDatabase({ databaseName, options, }?: DatabaseOption): Promise<import("mongodb").Db>;
export {};
//# sourceMappingURL=MongoDatabase.d.ts.map