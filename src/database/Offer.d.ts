import { MetadataForInterface } from "./DatabaseTemplate";
export interface IOffer {
    sellerId: string;
    sellerCastle: string;
    sellerName: string;
    item: string;
    price: number;
    qty: number;
}
export declare function parseOffer(data: IOffer): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: IOffer | MetadataForInterface<IOffer>): {
        getCollection(): Promise<import("mongodb").Collection<IOffer>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
        _findOne(filter: import("mongodb").FilterQuery<IOffer>, callback: import("mongodb").MongoCallback<IOffer | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<IOffer>, options?: import("mongodb").FindOneOptions | undefined): Promise<IOffer | null>;
        _findOne(filter: import("mongodb").FilterQuery<IOffer>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IOffer | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: import("mongodb").ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
        toSimpleObject(): Partial<IOffer>;
    };
    getCollection(): Promise<import("mongodb").Collection<IOffer>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, options?: import("mongodb").FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IOffer>, update: IOffer | import("mongodb").UpdateQuery<IOffer>, options: import("mongodb").FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
    _findOne(filter: import("mongodb").FilterQuery<IOffer>, callback: import("mongodb").MongoCallback<IOffer | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<IOffer>, options?: import("mongodb").FindOneOptions | undefined): Promise<IOffer | null>;
    _findOne(filter: import("mongodb").FilterQuery<IOffer>, options: import("mongodb").FindOneOptions, callback: import("mongodb").MongoCallback<IOffer | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IOffer>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: import("mongodb").ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IOffer>>): void;
};
export default class Offer extends BaseClass implements IOffer {
    sellerId: string;
    sellerCastle: string;
    sellerName: string;
    item: string;
    price: number;
    qty: number;
}
export {};
//# sourceMappingURL=Offer.d.ts.map