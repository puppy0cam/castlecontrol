/// <reference types="node" />
import { ChatWars } from "./chatwars";
import { EventEmitter } from "events";
export default function sendAPIRequest(request: ChatWars.API.Request): Promise<import("amqplib").Replies.Empty>;
export declare const GameEvents: ApiEvents;
declare class ApiEvents extends EventEmitter {
    on(event: "authAdditionalOperation", callback: (data: ChatWars.API.Response.authAdditionalOperation) => void): this;
    on(event: "authorizePayment", callback: (data: ChatWars.API.Response.authorizePayment) => void): this;
    on(event: "createAuthCode", callback: (data: ChatWars.API.Response.createAuthCode) => void): this;
    on(event: "getInfo", callback: (data: ChatWars.API.Response.getInfo) => void): this;
    on(event: "grantAdditionalOperation", callback: (data: ChatWars.API.Response.grantAdditionalOperation) => void): this;
    on(event: "grantToken", callback: (data: ChatWars.API.Response.grantToken) => void): this;
    on(event: "guildInfo", callback: (data: ChatWars.API.Response.guildInfo) => void): this;
    on(event: "pay", callback: (data: ChatWars.API.Response.pay) => void): this;
    on(event: "payout", callback: (data: ChatWars.API.Response.payout) => void): this;
    on(event: "requestBasicInfo", callback: (data: ChatWars.API.Response.requestBasicInfo) => void): this;
    on(event: "requestProfile", callback: (data: ChatWars.API.Response.requestProfile) => void): this;
    on(event: "requestStock", callback: (data: ChatWars.API.Response.requestStock) => void): this;
    on(event: "viewCraftbook", callback: (data: ChatWars.API.Response.viewCraftbook) => void): this;
    on(event: "wantToBuy", callback: (data: ChatWars.API.Response.wantToBuy) => void): this;
    on(event: "*", callback: (data: ChatWars.API.Response) => void): this;
    once(event: "authAdditionalOperation", callback: (data: ChatWars.API.Response.authAdditionalOperation) => void): this;
    once(event: "authorizePayment", callback: (data: ChatWars.API.Response.authorizePayment) => void): this;
    once(event: "createAuthCode", callback: (data: ChatWars.API.Response.createAuthCode) => void): this;
    once(event: "getInfo", callback: (data: ChatWars.API.Response.getInfo) => void): this;
    once(event: "grantAdditionalOperation", callback: (data: ChatWars.API.Response.grantAdditionalOperation) => void): this;
    once(event: "grantToken", callback: (data: ChatWars.API.Response.grantToken) => void): this;
    once(event: "guildInfo", callback: (data: ChatWars.API.Response.guildInfo) => void): this;
    once(event: "pay", callback: (data: ChatWars.API.Response.pay) => void): this;
    once(event: "payout", callback: (data: ChatWars.API.Response.payout) => void): this;
    once(event: "requestBasicInfo", callback: (data: ChatWars.API.Response.requestBasicInfo) => void): this;
    once(event: "requestProfile", callback: (data: ChatWars.API.Response.requestProfile) => void): this;
    once(event: "requestStock", callback: (data: ChatWars.API.Response.requestStock) => void): this;
    once(event: "viewCraftbook", callback: (data: ChatWars.API.Response.viewCraftbook) => void): this;
    once(event: "wantToBuy", callback: (data: ChatWars.API.Response.wantToBuy) => void): this;
    once(event: "*", callback: (data: ChatWars.API.Response) => void): this;
}
export {};
//# sourceMappingURL=GameApi.d.ts.map