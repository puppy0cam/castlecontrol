import { User } from "../telegram/types/User";
/**
 * the squad leaders.
 * These people can represent the castle for the squads.
 */
export declare const SQUAD_LEADER_USERS: User[];
//# sourceMappingURL=squad leader.d.ts.map