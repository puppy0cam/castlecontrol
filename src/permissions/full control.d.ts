import { User } from "../telegram/types/User";
/**
 * These users are assigned the full control role.
 * This gives them access to any and all functions no matter what.
 */
export declare const FULL_CONTROL_USERS: User[];
//# sourceMappingURL=full control.d.ts.map