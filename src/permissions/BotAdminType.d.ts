export declare enum BotAdminType {
    FULL_CONTROL = 0,
    COUNCIL = 1,
    SQUAD_LEADER = 2,
    SQUAD_DEPUTY = 3,
    USER = 4
}
//# sourceMappingURL=BotAdminType.d.ts.map