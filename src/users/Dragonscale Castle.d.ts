import { User } from "../telegram/types/User";
/**
 * A semi-official account belonging to the castle leaders.
 * Full rights should be granted due to their political power over the castle.
 */
export declare const DragonscaleCastle: User;
//# sourceMappingURL=Dragonscale Castle.d.ts.map