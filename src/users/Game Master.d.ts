import { User } from "../telegram/types/User";
/**
 * An official account belonging to the game developers.
 * Full rights should be granted despite not having official power over castle politics.
 * This only occurs as the game developers may require administrative control in some situations.
 * It is implied that this power will only be used for the purposes of game administration.
 */
export declare const GameMaster: User;
//# sourceMappingURL=Game Master.d.ts.map