import { User } from "../telegram/types/User";
/**
 * This account belongs to the bot creator.
 * Full rights should be granted despite not having official power over castle politics.
 * This only occurs because this user is the developer of the bot.
 * It is implied that this power will only be used for development purposes.
 */
export declare const AnimeSexStorm: User;
//# sourceMappingURL=Anime Sex Storm.d.ts.map