declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface pinChatMessage_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Identifier of a message to pin
     */
    message_id: Integer;
    /**
     * Pass True, if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels.
     */
    disable_notification?: boolean;
}
/**
 * Use this method to pin a message in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the ‘can_pin_messages’ admin right in the supergroup or ‘can_edit_messages’ admin right in the channel. Returns True on success.
 */
export declare type pinChatMessage = (options: pinChatMessage_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=pinChatMessage.d.ts.map