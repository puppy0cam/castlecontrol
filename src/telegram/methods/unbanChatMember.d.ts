declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface unbanChatMember_options {
    /**
     * Unique identifier for the target group or username of the target supergroup or channel (in the format @username)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
}
/**
 * Use this method to unban a previously kicked user in a supergroup or channel. The user will not return to the group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this to work. Returns True on success.
 */
export declare type unbanChatMember = (options: unbanChatMember_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=unbanChatMember.d.ts.map