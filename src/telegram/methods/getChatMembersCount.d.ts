declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface getChatMembersCount_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to get the number of members in a chat. Returns Int on success.
 */
export declare type getChatMembersCount = (options: getChatMembersCount_options) => Promise<Result<Integer>>;
export {};
//# sourceMappingURL=getChatMembersCount.d.ts.map