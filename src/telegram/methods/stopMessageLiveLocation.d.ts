import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
export interface stopMessageLiveLocation_options {
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id?: Integer | string;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
    /**
     * A JSON-serialized object for a new inline keyboard.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to stop updating a live location message sent by the bot or via the bot (for inline bots) before live_period expires. On success, if the message was sent by the bot, the sent Message is returned, otherwise True is returned.
 */
export declare type stopMessageLiveLocation = (options: stopMessageLiveLocation_options) => Promise<Result<Message | True>>;
//# sourceMappingURL=stopMessageLiveLocation.d.ts.map