import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
export interface editMessageReplyMarkup_options {
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id?: Integer | string;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
    /**
     * A JSON-serialized object for an inline keyboard.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to edit only the reply markup of messages sent by the bot or via the bot (for inline bots).  On success, if edited message is sent by the bot, the edited Message is returned, otherwise True is returned.
 */
export declare type editMessageReplyMarkup = (options: editMessageReplyMarkup_options) => Promise<Result<Message | True>>;
//# sourceMappingURL=editMessageReplyMarkup.d.ts.map