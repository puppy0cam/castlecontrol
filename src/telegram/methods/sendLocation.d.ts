declare type Float = number;
declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ForceReply } from "../types/ForceReply";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
import { ReplyKeyboardMarkup } from "../types/ReplyKeyboardMarkup";
import { ReplyKeyboardRemove } from "../types/ReplyKeyboardRemove";
export interface sendLocation_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Latitude of the location
     */
    latitude: Float;
    /**
     * Longitude of the location
     */
    longitude: Float;
    /**
     * Period in seconds for which the location will be updated (see Live Locations, should be between 60 and 86400.
     */
    live_period?: Integer;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
     */
    reply_markup?: InlineKeyboardMarkup | ReplyKeyboardMarkup | ReplyKeyboardRemove | ForceReply;
}
/**
 * Use this method to send point on the map. On success, the sent Message is returned.
 */
export declare type sendLocation = (options: sendLocation_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendLocation.d.ts.map