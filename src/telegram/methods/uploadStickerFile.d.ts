declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { File } from "../types/File";
import { InputFile } from "../types/InputFile";
export interface uploadStickerFile_options {
    /**
     * User identifier of sticker file owner
     */
    user_id: Integer;
    /**
     * Png image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. More info on Sending Files »
     */
    png_sticker: InputFile;
}
/**
 * Use this method to upload a .png file with a sticker for later use in createNewStickerSet and addStickerToSet methods (can be used multiple times). Returns the uploaded File on success.
 */
export declare type uploadStickerFile = (options: uploadStickerFile_options) => Promise<Result<File>>;
export {};
//# sourceMappingURL=uploadStickerFile.d.ts.map