import { Result } from "../../bot/TelegramBot";
import { User } from "../types/User";
export interface getMe_options {
}
/**
 * A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.
 */
export declare type getMe = (options: getMe_options) => Promise<Result<User>>;
//# sourceMappingURL=getMe.d.ts.map