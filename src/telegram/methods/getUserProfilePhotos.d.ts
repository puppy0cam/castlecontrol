declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { UserProfilePhotos } from "../types/UserProfilePhotos";
export interface getUserProfilePhotos_options {
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
    /**
     * Sequential number of the first photo to be returned. By default, all photos are returned.
     */
    offset?: Integer;
    /**
     * Limits the number of photos to be retrieved. Values between 1—100 are accepted. Defaults to 100.
     */
    limit?: Integer;
}
/**
 * Use this method to get a list of profile pictures for a user. Returns a UserProfilePhotos object.
 */
export declare type getUserProfilePhotos = (options: getUserProfilePhotos_options) => Promise<Result<UserProfilePhotos>>;
export {};
//# sourceMappingURL=getUserProfilePhotos.d.ts.map