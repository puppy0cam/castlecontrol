declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface restrictChatMember_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
    /**
     * Date when restrictions will be lifted for the user, unix time. If user is restricted for more than 366 days or less than 30 seconds from the current time, they are considered to be restricted forever
     */
    until_date?: Integer;
    /**
     * Pass True, if the user can send text messages, contacts, locations and venues
     */
    can_send_messages?: boolean;
    /**
     * Pass True, if the user can send audios, documents, photos, videos, video notes and voice notes, implies can_send_messages
     */
    can_send_media_messages?: boolean;
    /**
     * Pass True, if the user can send animations, games, stickers and use inline bots, implies can_send_media_messages
     */
    can_send_other_messages?: boolean;
    /**
     * Pass True, if the user may add web page previews to their messages, implies can_send_media_messages
     */
    can_add_web_page_previews?: boolean;
}
/**
 * Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this to work and must have the appropriate admin rights. Pass True for all boolean parameters to lift restrictions from a user. Returns True on success.
 */
export declare type restrictChatMember = (options: restrictChatMember_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=restrictChatMember.d.ts.map