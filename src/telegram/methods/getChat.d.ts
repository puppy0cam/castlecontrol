declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { Chat } from "../types/Chat";
export interface getChat_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.). Returns a Chat object on success.
 */
export declare type getChat = (options: getChat_options) => Promise<Result<Chat>>;
export {};
//# sourceMappingURL=getChat.d.ts.map