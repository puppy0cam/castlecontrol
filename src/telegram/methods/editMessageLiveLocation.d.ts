import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
export interface editMessageLiveLocation_options {
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id?: Integer | string;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
    /**
     * Latitude of new location
     */
    latitude: Float;
    /**
     * Longitude of new location
     */
    longitude: Float;
    /**
     * A JSON-serialized object for a new inline keyboard.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to edit live location messages sent by the bot or via the bot (for inline bots). A location can be edited until its live_period expires or editing is explicitly disabled by a call to stopMessageLiveLocation. On success, if the edited message was sent by the bot, the edited Message is returned, otherwise True is returned.
 */
export declare type editMessageLiveLocation = (options: editMessageLiveLocation_options) => Promise<Result<Message | Bool>>;
//# sourceMappingURL=editMessageLiveLocation.d.ts.map