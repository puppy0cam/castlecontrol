import { Result } from "../../bot/TelegramBot";
import { WebhookInfo } from "../types/WebhookInfo";
export interface getWebhookInfo_options {
}
/**
 * Use this method to get current webhook status. Requires no parameters. On success, returns a WebhookInfo object. If the bot is using getUpdates, will return an object with the url field empty.
 */
export declare type getWebhookInfo = (options: getWebhookInfo_options) => Promise<Result<WebhookInfo>>;
//# sourceMappingURL=getWebhookInfo.d.ts.map