declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface deleteChatPhoto_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to delete a chat photo. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
 */
export declare type deleteChatPhoto = (options: deleteChatPhoto_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=deleteChatPhoto.d.ts.map