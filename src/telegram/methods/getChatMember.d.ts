declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ChatMember } from "../types/ChatMember";
export interface getChatMember_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
}
/**
 * Use this method to get information about a member of a chat. Returns a ChatMember object on success.
 */
export declare type getChatMember = (options: getChatMember_options) => Promise<Result<ChatMember>>;
export {};
//# sourceMappingURL=getChatMember.d.ts.map