import { Result } from "../../bot/TelegramBot";
import { Message } from "../types/Message";
export interface setGameScore_options {
    /**
     * User identifier
     */
    user_id: Integer;
    /**
     * New score, must be non-negative
     */
    score: Integer;
    /**
     * Pass True, if the high score is allowed to decrease. This can be useful when fixing mistakes or banning cheaters
     */
    force?: boolean;
    /**
     * Pass True, if the game message should not be automatically edited to include the current scoreboard
     */
    disable_edit_message?: boolean;
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat
     */
    chat_id?: Integer;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
}
/**
 * Use this method to set the score of the specified user in a game. On success, if the message was sent by the bot, returns the edited Message, otherwise returns True. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
 */
export declare type setGameScore = (options: setGameScore_options) => Promise<Result<Message | True>>;
//# sourceMappingURL=setGameScore.d.ts.map