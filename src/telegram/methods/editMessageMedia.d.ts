import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { InputMedia } from "../types/InputMedia";
import { Message } from "../types/Message";
export interface editMessageMedia_options {
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id?: Integer | string;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
    /**
     * A JSON-serialized object for a new media content of the message
     */
    media: InputMedia;
    /**
     * A JSON-serialized object for a new inline keyboard.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to edit audio, document, photo, or video messages. If a message is a part of a message album, then it can be edited only to a photo or a video. Otherwise, message type can be changed arbitrarily. When inline message is edited, new file can't be uploaded. Use previously uploaded file via its file_id or specify a URL. On success, if the edited message was sent by the bot, the edited Message is returned, otherwise True is returned.
 */
export declare type editMessageMedia = (options: editMessageMedia_options) => Promise<Result<Message | True>>;
//# sourceMappingURL=editMessageMedia.d.ts.map