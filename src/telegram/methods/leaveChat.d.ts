declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface leaveChat_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method for your bot to leave a group, supergroup or channel. Returns True on success.
 */
export declare type leaveChat = (options: leaveChat_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=leaveChat.d.ts.map