import { Result } from "../../bot/TelegramBot";
export interface deleteWebhook_options {
}
/**
 * Use this method to remove webhook integration if you decide to switch back to getUpdates. Returns True on success. Requires no parameters.
 */
export declare type deleteWebhook = (options: deleteWebhook_options) => Promise<Result<boolean>>;
//# sourceMappingURL=deleteWebhook.d.ts.map