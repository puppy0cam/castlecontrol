declare type Float = number;
declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ForceReply } from "../types/ForceReply";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
import { ReplyKeyboardMarkup } from "../types/ReplyKeyboardMarkup";
import { ReplyKeyboardRemove } from "../types/ReplyKeyboardRemove";
export interface sendVenue_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Latitude of the venue
     */
    latitude: Float;
    /**
     * Longitude of the venue
     */
    longitude: Float;
    /**
     * Name of the venue
     */
    title: string;
    /**
     * Address of the venue
     */
    address: string;
    /**
     * Foursquare identifier of the venue
     */
    foursquare_id?: string;
    /**
     * Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
     */
    foursquare_type?: string;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
     */
    reply_markup?: InlineKeyboardMarkup | ReplyKeyboardMarkup | ReplyKeyboardRemove | ForceReply;
}
/**
 * Use this method to send information about a venue. On success, the sent Message is returned.
 */
export declare type sendVenue = (options: sendVenue_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendVenue.d.ts.map