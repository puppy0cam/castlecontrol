declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { Message } from "../types/Message";
export interface forwardMessage_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier for the chat where the original message was sent (or channel username in the format @channelusername)
     */
    from_chat_id: Integer | string;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * Message identifier in the chat specified in from_chat_id
     */
    message_id: Integer;
}
/**
 * Use this method to forward messages of any kind. On success, the sent Message is returned.
 */
export declare type forwardMessage = (options: forwardMessage_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=forwardMessage.d.ts.map