declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { PassportElementError } from "../types/PassportElementError";
export interface setPassportDataErrors_options {
    /**
     * User identifier
     */
    user_id: Integer;
    /**
     * A JSON-serialized array describing the errors
     */
    errors: PassportElementError[];
}
/**
 * Informs a user that some of the Telegram Passport elements they provided contains errors. The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change). Returns True on success.
 */
export declare type setPassportDataErrors = (options: setPassportDataErrors_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=setPassportDataErrors.d.ts.map