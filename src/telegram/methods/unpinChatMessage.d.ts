declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface unpinChatMessage_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to unpin a message in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the ‘can_pin_messages’ admin right in the supergroup or ‘can_edit_messages’ admin right in the channel. Returns True on success.
 */
export declare type unpinChatMessage = (options: unpinChatMessage_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=unpinChatMessage.d.ts.map