declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { GameHighScore } from "../types/GameHighScore";
export interface getGameHighScores_options {
    /**
     * Target user id
     */
    user_id: Integer;
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat
     */
    chat_id?: Integer;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
}
/**
 * Use this method to get data for high score tables. Will return the score of the specified user and several of his neighbors in a game. On success, returns an Array of GameHighScore objects.
 */
export declare type getGameHighScores = (options: getGameHighScores_options) => Promise<Result<GameHighScore[]>>;
export {};
//# sourceMappingURL=getGameHighScores.d.ts.map