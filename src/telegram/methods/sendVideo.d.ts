declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ForceReply } from "../types/ForceReply";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { InputFile } from "../types/InputFile";
import { Message } from "../types/Message";
import { ReplyKeyboardMarkup } from "../types/ReplyKeyboardMarkup";
import { ReplyKeyboardRemove } from "../types/ReplyKeyboardRemove";
export interface sendVideo_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Video to send. Pass a file_id as string to send a video that exists on the Telegram servers (recommended), pass an HTTP URL as a string for Telegram to get a video from the Internet, or upload a new video using multipart/form-data. More info on Sending Files »
     */
    video: InputFile | string;
    /**
     * Duration of sent video in seconds
     */
    duration?: Integer;
    /**
     * Video width
     */
    width?: Integer;
    /**
     * Video height
     */
    height?: Integer;
    /**
     * Thumbnail of the file sent. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail‘s width and height should not exceed 90. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can’t be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
     */
    thumb?: InputFile | string;
    /**
     * Video caption (may also be used when resending videos by file_id), 0-200 characters
     */
    caption?: string;
    /**
     * Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in the media caption.
     */
    parse_mode?: string;
    /**
     * Pass True, if the uploaded video is suitable for streaming
     */
    supports_streaming?: boolean;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
     */
    reply_markup?: InlineKeyboardMarkup | ReplyKeyboardMarkup | ReplyKeyboardRemove | ForceReply;
}
/**
 * Use this method to send video files, Telegram clients support mp4 videos (other formats may be sent as Document). On success, the sent Message is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
 */
export declare type sendVideo = (options: sendVideo_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendVideo.d.ts.map