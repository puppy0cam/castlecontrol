import { Result } from "../../bot/TelegramBot";
export interface deleteStickerFromSet_options {
    /**
     * File identifier of the sticker
     */
    sticker: string;
}
/**
 * Use this method to delete a sticker from a set created by the bot. Returns True on success.
 */
export declare type deleteStickerFromSet = (options: deleteStickerFromSet_options) => Promise<Result<boolean>>;
//# sourceMappingURL=deleteStickerFromSet.d.ts.map