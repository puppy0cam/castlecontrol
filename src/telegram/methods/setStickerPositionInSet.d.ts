declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface setStickerPositionInSet_options {
    /**
     * File identifier of the sticker
     */
    sticker: string;
    /**
     * New sticker position in the set, zero-based
     */
    position: Integer;
}
/**
 * Use this method to move a sticker in a set created by the bot to a specific position . Returns True on success.
 */
export declare type setStickerPositionInSet = (options: setStickerPositionInSet_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=setStickerPositionInSet.d.ts.map