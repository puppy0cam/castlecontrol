declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InputMediaPhoto } from "../types/InputMediaPhoto";
import { InputMediaVideo } from "../types/InputMediaVideo";
import { Message } from "../types/Message";
export interface sendMediaGroup_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * A JSON-serialized array describing photos and videos to be sent, must include 2–10 items
     */
    media: Array<InputMediaPhoto | InputMediaVideo>;
    /**
     * Sends the messages silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the messages are a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
}
/**
 * Use this method to send a group of photos or videos as an album. On success, an array of the sent Messages is returned.
 */
export declare type sendMediaGroup = (options: sendMediaGroup_options) => Promise<Result<Message[]>>;
export {};
//# sourceMappingURL=sendMediaGroup.d.ts.map