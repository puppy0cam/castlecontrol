declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { LabeledPrice } from "../types/LabeledPrice";
import { Message } from "../types/Message";
export interface sendInvoice_options {
    /**
     * Unique identifier for the target private chat
     */
    chat_id: Integer;
    /**
     * Product name, 1-32 characters
     */
    title: string;
    /**
     * Product description, 1-255 characters
     */
    description: string;
    /**
     * Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
     */
    payload: string;
    /**
     * Payments provider token, obtained via Botfather
     */
    provider_token: string;
    /**
     * Unique deep-linking parameter that can be used to generate this invoice when used as a start parameter
     */
    start_parameter: string;
    /**
     * Three-letter ISO 4217 currency code, see more on currencies
     */
    currency: string;
    /**
     * Price breakdown, a list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
     */
    prices: LabeledPrice[];
    /**
     * JSON-encoded data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
     */
    provider_data?: string;
    /**
     * URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service. People like it better when they see what they are paying for.
     */
    photo_url?: string;
    /**
     * Photo size
     */
    photo_size?: Integer;
    /**
     * Photo width
     */
    photo_width?: Integer;
    /**
     * Photo height
     */
    photo_height?: Integer;
    /**
     * Pass True, if you require the user's full name to complete the order
     */
    need_name?: boolean;
    /**
     * Pass True, if you require the user's phone number to complete the order
     */
    need_phone_number?: boolean;
    /**
     * Pass True, if you require the user's email address to complete the order
     */
    need_email?: boolean;
    /**
     * Pass True, if you require the user's shipping address to complete the order
     */
    need_shipping_address?: boolean;
    /**
     * Pass True, if user's phone number should be sent to provider
     */
    send_phone_number_to_provider?: boolean;
    /**
     * Pass True, if user's email address should be sent to provider
     */
    send_email_to_provider?: boolean;
    /**
     * Pass True, if the final price depends on the shipping method
     */
    is_flexible?: boolean;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to send invoices. On success, the sent Message is returned.
 */
export declare type sendInvoice = (options: sendInvoice_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendInvoice.d.ts.map