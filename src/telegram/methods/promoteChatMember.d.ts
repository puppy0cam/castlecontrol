declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface promoteChatMember_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
    /**
     * Pass True, if the administrator can change chat title, photo and other settings
     */
    can_change_info?: boolean;
    /**
     * Pass True, if the administrator can create channel posts, channels only
     */
    can_post_messages?: boolean;
    /**
     * Pass True, if the administrator can edit messages of other users and can pin messages, channels only
     */
    can_edit_messages?: boolean;
    /**
     * Pass True, if the administrator can delete messages of other users
     */
    can_delete_messages?: boolean;
    /**
     * Pass True, if the administrator can invite new users to the chat
     */
    can_invite_users?: boolean;
    /**
     * Pass True, if the administrator can restrict, ban or unban chat members
     */
    can_restrict_members?: boolean;
    /**
     * Pass True, if the administrator can pin messages, supergroups only
     */
    can_pin_messages?: boolean;
    /**
     * Pass True, if the administrator can add new administrators with a subset of his own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by him)
     */
    can_promote_members?: boolean;
}
/**
 * Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Pass False for all boolean parameters to demote a user. Returns True on success.
 */
export declare type promoteChatMember = (options: promoteChatMember_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=promoteChatMember.d.ts.map