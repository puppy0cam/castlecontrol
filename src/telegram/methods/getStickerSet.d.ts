import { Result } from "../../bot/TelegramBot";
import { StickerSet } from "../types/StickerSet";
export interface getStickerSet_options {
    /**
     * Name of the sticker set
     */
    name: string;
}
/**
 * Use this method to get a sticker set. On success, a StickerSet object is returned.
 */
export declare type getStickerSet = (options: getStickerSet_options) => Promise<Result<StickerSet>>;
//# sourceMappingURL=getStickerSet.d.ts.map