declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InputFile } from "../types/InputFile";
import { MaskPosition } from "../types/MaskPosition";
export interface addStickerToSet_options {
    /**
     * User identifier of sticker set owner
     */
    user_id: Integer;
    /**
     * Sticker set name
     */
    name: string;
    /**
     * Png image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. Pass a file_id as a string to send a file that already exists on the Telegram servers, pass an HTTP URL as a string for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
     */
    png_sticker: InputFile | string;
    /**
     * One or more emoji corresponding to the sticker
     */
    emojis: string;
    /**
     * A JSON-serialized object for position where the mask should be placed on faces
     */
    mask_position?: MaskPosition;
}
/**
 * Use this method to add a new sticker to a set created by the bot. Returns True on success.
 */
export declare type addStickerToSet = (options: addStickerToSet_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=addStickerToSet.d.ts.map