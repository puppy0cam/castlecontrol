declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
export interface sendGame_options {
    /**
     * Unique identifier for the target chat
     */
    chat_id: Integer;
    /**
     * Short name of the game, serves as the unique identifier for the game. Set up your games via Botfather.
     */
    game_short_name: string;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * A JSON-serialized object for an inline keyboard. If empty, one ‘Play game_title’ button will be shown. If not empty, the first button must launch the game.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to send a game. On success, the sent Message is returned.
 */
export declare type sendGame = (options: sendGame_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendGame.d.ts.map