declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InputFile } from "../types/InputFile";
import { MaskPosition } from "../types/MaskPosition";
export interface createNewStickerSet_options {
    /**
     * User identifier of created sticker set owner
     */
    user_id: Integer;
    /**
     * Short name of sticker set, to be used in t.me/addstickers/ URLs (e.g., animals). Can contain only english letters, digits and underscores. Must begin with a letter, can't contain consecutive underscores and must end in “_by_<bot username>”. <bot_username> is case insensitive. 1-64 characters.
     */
    name: string;
    /**
     * Sticker set title, 1-64 characters
     */
    title: string;
    /**
     * Png image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. Pass a file_id as a string to send a file that already exists on the Telegram servers, pass an HTTP URL as a string for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
     */
    png_sticker: InputFile | string;
    /**
     * One or more emoji corresponding to the sticker
     */
    emojis: string;
    /**
     * Pass True, if a set of mask stickers should be created
     */
    contains_masks?: boolean;
    /**
     * A JSON-serialized object for position where the mask should be placed on faces
     */
    mask_position?: MaskPosition;
}
/**
 * Use this method to create new sticker set owned by a user. The bot will be able to edit the created sticker set. Returns True on success.
 */
export declare type createNewStickerSet = (options: createNewStickerSet_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=createNewStickerSet.d.ts.map