declare type PromisedDatabaseMethod<Type> = Promise<DatabaseMethod<Type>>;
declare type DatabaseMethod<Type> = Type | Type[];
declare type Integer = number;
declare type Bool = boolean;
declare type Float = number;
declare type True = true;
declare type False = false;
//# sourceMappingURL=special_types.d.ts.map