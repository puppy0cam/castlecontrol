declare type Integer = number;
import { PhotoSize } from "./PhotoSize";
/**
 * This object represent a user's profile pictures.
 */
export interface UserProfilePhotos {
    /**
     * Total number of profile pictures the target user has
     */
    total_count: Integer;
    /**
     * Requested profile pictures (in up to 4 sizes each)
     */
    photos: PhotoSize[][];
}
export declare class UserProfilePhotos implements UserProfilePhotos {
    constructor(data: UserProfilePhotos);
}
export {};
//# sourceMappingURL=UserProfilePhotos.d.ts.map