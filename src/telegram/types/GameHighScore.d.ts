declare type Integer = number;
import { User } from "./User";
/**
 * This object represents one row of the high scores table for a game.
 */
export interface GameHighScore {
    /**
     * Position in high score table for the game
     */
    position: Integer;
    /**
     * User
     */
    user: User;
    /**
     * Score
     */
    score: Integer;
}
export declare class GameHighScore implements GameHighScore {
    constructor(data: GameHighScore);
}
export {};
//# sourceMappingURL=GameHighScore.d.ts.map