declare type Integer = number;
import { Animation } from "./Animation";
import { Audio } from "./Audio";
import { Chat } from "./Chat";
import { Contact } from "./Contact";
import { Document } from "./Document";
import { Game } from "./Game";
import { Invoice } from "./Invoice";
import { Location } from "./Location";
import { MessageEntity } from "./MessageEntity";
import { PassportData } from "./PassportData";
import { PhotoSize } from "./PhotoSize";
import { Sticker } from "./Sticker";
import { SuccessfulPayment } from "./SuccessfulPayment";
import { User } from "./User";
import { Video } from "./Video";
import { VideoNote } from "./VideoNote";
import { Voice } from "./Voice";
/**
 * This object represents a message.
 */
export interface Message {
    /**
     * Unique message identifier inside this chat
     */
    message_id: Integer;
    /**
     * Optional. Sender, empty for messages sent to channels
     */
    from?: User;
    /**
     * Date the message was sent in Unix time
     */
    date: Integer | Date;
    /**
     * Conversation the message belongs to
     */
    chat: Chat;
    /**
     * Optional. For forwarded messages, sender of the original message
     */
    forward_from?: User;
    /**
     * Optional. For messages forwarded from channels, information about the original channel
     */
    forward_from_chat?: Chat;
    /**
     * Optional. For messages forwarded from channels, identifier of the original message in the channel
     */
    forward_from_message_id?: Integer;
    /**
     * Optional. For messages forwarded from channels, signature of the post author if present
     */
    forward_signature?: string;
    /**
     * Optional. For forwarded messages, date the original message was sent in Unix time
     */
    forward_date?: Integer | Date;
    /**
     * Optional. For replies, the original message. Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
     */
    reply_to_message?: Message;
    /**
     * Optional. Date the message was last edited in Unix time
     */
    edit_date?: Integer | Date;
    /**
     * Optional. The unique identifier of a media message group this message belongs to
     */
    media_group_id?: string;
    /**
     * Optional. Signature of the post author for messages in channels
     */
    author_signature?: string;
    /**
     * Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters.
     */
    text?: string;
    /**
     * Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
     */
    entities?: MessageEntity[];
    /**
     * Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
     */
    caption_entities?: MessageEntity[];
    /**
     * Optional. Message is an audio file, information about the file
     */
    audio?: Audio;
    /**
     * Optional. Message is a general file, information about the file
     */
    document?: Document;
    /**
     * Optional. Message is an animation, information about the animation. For backward compatibility, when this field is set, the document field will also be set
     */
    animation?: Animation;
    /**
     * Optional. Message is a game, information about the game.
     */
    game?: Game;
    /**
     * Optional. Message is a photo, available sizes of the photo
     */
    photo?: PhotoSize[];
    /**
     * Optional. Message is a sticker, information about the sticker
     */
    sticker?: Sticker;
    /**
     * Optional. Message is a video, information about the video
     */
    video?: Video;
    /**
     * Optional. Message is a voice message, information about the file
     */
    voice?: Voice;
    /**
     * Optional. Message is a video note, information about the video message
     */
    video_note?: VideoNote;
    /**
     * Optional. Caption for the audio, document, photo, video or voice, 0-200 characters
     */
    caption?: string;
    /**
     * Optional. Message is a shared contact, information about the contact
     */
    contact?: Contact;
    /**
     * Optional. Message is a shared location, information about the location
     */
    location?: Location;
    /**
     * Optional. New members that were added to the group or supergroup and information about them (the bot itself may be one of these members)
     */
    new_chat_members?: User[];
    /**
     * Optional. A member was removed from the group, information about them (this member may be the bot itself)
     */
    left_chat_member?: User;
    /**
     * Optional. A chat title was changed to this value
     */
    new_chat_title?: string;
    /**
     * Optional. Specified message was pinned. Note that the Message object in this field will not contain further reply_to_message fields even if it is itself a reply.
     */
    pinned_message?: Message;
    /**
     * Optional. Message is an invoice for a payment, information about the invoice.
     */
    invoice?: Invoice;
    /**
     * Optional. Message is a service message about a successful payment, information about the payment.
     */
    successful_payment?: SuccessfulPayment;
    /**
     * Optional. The domain name of the website on which the user has logged in.
     */
    connected_website?: string;
    /**
     * Optional. Telegram Passport data
     */
    passport_data?: PassportData;
}
export declare class Message implements Message {
    constructor(data: Message);
}
export declare function isMessage(message: any): message is Message;
export {};
//# sourceMappingURL=Message.d.ts.map