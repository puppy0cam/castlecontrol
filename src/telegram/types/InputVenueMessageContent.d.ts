declare type Float = number;
/**
 * Represents the content of a venue message to be sent as the result of an inline query.
 */
export interface InputVenueMessageContent {
    /**
     * Latitude of the venue in degrees
     */
    latitude: Float;
    /**
     * Longitude of the venue in degrees
     */
    longitude: Float;
    /**
     * Name of the venue
     */
    title: string;
    /**
     * Address of the venue
     */
    address: string;
    /**
     * Optional. Foursquare identifier of the venue, if known
     */
    foursquare_id?: string;
    /**
     * Optional. Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
     */
    foursquare_type?: string;
}
export declare class InputVenueMessageContent implements InputVenueMessageContent {
    constructor(data: InputVenueMessageContent);
}
export {};
//# sourceMappingURL=InputVenueMessageContent.d.ts.map