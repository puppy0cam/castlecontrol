import { InputMediaAnimation } from "./InputMediaAnimation";
import { InputMediaAudio } from "./InputMediaAudio";
import { InputMediaDocument } from "./InputMediaDocument";
import { InputMediaPhoto } from "./InputMediaPhoto";
import { InputMediaVideo } from "./InputMediaVideo";
export declare type InputMedia = InputMediaAnimation | InputMediaDocument | InputMediaAudio | InputMediaPhoto | InputMediaVideo;
//# sourceMappingURL=InputMedia.d.ts.map