declare type Float = number;
declare type Integer = number;
/**
 * Represents the content of a location message to be sent as the result of an inline query.
 */
export interface InputLocationMessageContent {
    /**
     * Latitude of the location in degrees
     */
    latitude: Float;
    /**
     * Longitude of the location in degrees
     */
    longitude: Float;
    /**
     * Optional. Period in seconds for which the location can be updated, should be between 60 and 86400.
     */
    live_period?: Integer;
}
export declare class InputLocationMessageContent implements InputLocationMessageContent {
    constructor(data: InputLocationMessageContent);
}
export {};
//# sourceMappingURL=InputLocationMessageContent.d.ts.map