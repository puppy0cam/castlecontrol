declare type Integer = number;
/**
 * This object represents a file uploaded to Telegram Passport. Currently all Telegram Passport files are in JPEG format when decrypted and don't exceed 10MB.
 */
export interface PassportFile {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * File size
     */
    file_size: Integer;
    /**
     * Unix time when the file was uploaded
     */
    file_date: Integer | Date;
}
export declare class PassportFile implements PassportFile {
    constructor(data: PassportFile);
}
export {};
//# sourceMappingURL=PassportFile.d.ts.map