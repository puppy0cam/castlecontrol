declare type Integer = number;
import { PhotoSize } from "./PhotoSize";
/**
 * This object represents a video file.
 */
export interface Video {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Video width as defined by sender
     */
    width: Integer;
    /**
     * Video height as defined by sender
     */
    height: Integer;
    /**
     * Duration of the video in seconds as defined by sender
     */
    duration: Integer;
    /**
     * Optional. Video thumbnail
     */
    thumb?: PhotoSize;
    /**
     * Optional. Mime type of a file as defined by sender
     */
    mime_type?: string;
    /**
     * Optional. File size
     */
    file_size?: Integer;
}
export declare class Video implements Video {
    constructor(data: Video);
}
export {};
//# sourceMappingURL=Video.d.ts.map