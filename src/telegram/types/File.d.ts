declare type Integer = number;
/**
 * This object represents a file ready to be downloaded. The file can be downloaded via the link https://api.telegram.org/file/bot<token>/<file_path>. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile.
 */
export interface File {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Optional. File size, if known
     */
    file_size?: Integer;
    /**
     * Optional. File path. Use https://api.telegram.org/file/bot<token>/<file_path> to get the file.
     */
    file_path?: string;
}
export declare class File implements File {
    constructor(data: File);
}
export {};
//# sourceMappingURL=File.d.ts.map