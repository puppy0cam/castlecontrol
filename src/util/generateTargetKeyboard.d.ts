import { InlineKeyboardButton } from "../telegram/types/InlineKeyboardButton";
export declare type availableTargets = "🥔" | "🦅" | "🌑" | "🐺" | "🦈" | "🦌" | "🛏" | "🎖" | "🛡";
export declare const targetKeyboardTemplate: availableTargets[][];
export default function generateTargetKeyboard(battleId: number): InlineKeyboardButton[][];
//# sourceMappingURL=generateTargetKeyboard.d.ts.map