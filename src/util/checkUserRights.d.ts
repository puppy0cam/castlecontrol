import { ChatMember } from "../telegram/types/ChatMember";
export declare type userRightsTypes = "can_be_edited" | "can_change_info" | "can_delete_messages" | "can_edit_messages" | "can_invite_users" | "can_pin_messages" | "can_post_messages" | "can_promote_members" | "can_restrict_members" | "can_send_media_messages" | "can_send_messages" | "can_send_other_messages";
export default function checkUserRights(user: ChatMember, right: userRightsTypes): boolean;
//# sourceMappingURL=checkUserRights.d.ts.map