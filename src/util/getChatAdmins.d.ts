import { ChatMember } from "../telegram/types/ChatMember";
export default function getChatAdmins(chat: number): Promise<ChatMember[]>;
//# sourceMappingURL=getChatAdmins.d.ts.map