export declare function ensurePromiseIsComplete(promise: Promise<any>): Promise<void>;
export declare function ensureAllPromisesAreDone(...promises: Array<Promise<any>>): Promise<typeof promises>;
//# sourceMappingURL=promiseKeeper.d.ts.map