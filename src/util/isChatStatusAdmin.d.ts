import { ChatMember } from "../telegram/types/ChatMember";
export default function isChatStatusAdmin(status: ChatMember): boolean;
//# sourceMappingURL=isChatStatusAdmin.d.ts.map