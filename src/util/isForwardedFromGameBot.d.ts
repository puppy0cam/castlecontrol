import { Update } from "../telegram/types/Update";
import { Message } from "../telegram/types/Message";
export default function isMessageForwardedFromGameBot(data: Update | Message): boolean;
//# sourceMappingURL=isForwardedFromGameBot.d.ts.map