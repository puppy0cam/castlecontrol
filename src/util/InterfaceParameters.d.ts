export declare type typeofResult = "boolean" | "function" | "number" | "object" | "string" | "symbol" | "undefined";
export interface DynamicValues {
    [value: string]: any;
}
interface CheckerThing {
    [key: string]: possibilities;
}
type possibilities = ValueTypes.possibleReferences | ValueTypes.exemptionsFromObjectSpread;
export declare type InterfaceValues<T extends CheckerThing> = {
    [key in keyof T]: T[key] extends ValueTypes.exemptionsFromObjectSpread ? PossibleArray<T[key] | ConstructorBuilder<T[key]>> : T[key] extends CheckerThing ? InterfaceValues<T[key]> : PossibleArray<((data: T[key]) => boolean) | T[key] | ConstructorBuilder<T[key]> | typeofResult>;
};
type PossibleArray<T> = T[] | T;
interface ConstructorBuilder<T> extends Function {
    prototype: T;
}
export interface InterfaceParameters<T extends typeofResult | ValueTypes.exemptionsFromObjectSpread> {
    type: T;
    additionalChecks: Array<(value: ValueTypes.refersTo<T>) => boolean>;
}
export declare function verifyObject<T extends CheckerThing>(value: T, checker: InterfaceValues<T>): value is T;
export declare namespace ValueTypes {
    type names = boo.name | fun.name | num.name | obj.name | str.name | sym.name | und.name;
    type identifiers = boo.identifier | fun.identifier | num.identifier | obj.identifier | str.identifier | sym.identifier | und.identifier;
    type possibleReferences = boo.refersTo | fun.refersTo | num.refersTo | obj.refersTo | str.refersTo | sym.refersTo | und.refersTo;
    type name<T extends exemptionsFromObjectSpread | identifiers> = T extends boo ? boo.name : T extends fun ? fun.name : T extends num ? num.name : T extends obj ? obj.name : T extends str ? str.name : T extends sym ? sym.name : T extends und ? und.name : T;
    type identifier<T extends exemptionsFromObjectSpread | names> = T extends boo.name ? boo.identifier : T extends fun.name ? fun.identifier : T extends num.name ? num.identifier : T extends obj ? obj.identifier : T extends str.name ? str.identifier : T extends sym.name ? sym.identifier : T extends und.name ? und.identifier : T;
    type refersTo<T extends exemptionsFromObjectSpread | names | identifiers> = T extends boo | boo.name ? boo.refersTo : T extends fun | fun.name ? fun.refersTo : T extends num | num.name ? num.refersTo : T extends obj | obj.name ? obj.refersTo : T extends str | str.name ? str.refersTo : T extends sym | sym.name ? sym.refersTo : T extends und | und.name ? und.refersTo : T;
    type fromRawType<T extends exemptionsFromObjectSpread | possibleReferences> = T extends boo.refersTo ? boo.identifier : T extends fun.refersTo ? fun.identifier : T extends num.refersTo ? num.identifier : T extends obj.refersTo ? obj.identifier : T extends str.refersTo ? str.identifier : T extends sym.refersTo ? sym.identifier : T extends und.refersTo ? und.identifier : T;
    type exemptionsFromObjectSpread = Date | null;
    type SetupSwapper<Value extends A | B, A, B> = Value extends A ? B : A;
    namespace boo {
        const name: name;
        const identifier: unique symbol;
        type name = "boolean";
        type identifier = typeof identifier;
        type refersTo = boolean;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type boo = typeof boo.identifier;
    namespace fun {
        const name: name;
        const identifier: unique symbol;
        type name = "function";
        type identifier = typeof identifier;
        type refersTo = Function | ((...args: [...any[]]) => any);
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type fun = typeof fun.identifier;
    namespace num {
        const name: name;
        const identifier: unique symbol;
        type name = "number";
        type identifier = typeof identifier;
        type refersTo = number;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type num = typeof num.identifier;
    namespace obj {
        const name: name;
        const identifier: unique symbol;
        type name = "object";
        type identifier = typeof identifier;
        type refersTo = object;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type obj = typeof obj.identifier;
    namespace str {
        const name: name;
        const identifier: unique symbol;
        type name = "string";
        type identifier = typeof identifier;
        type refersTo = string;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type str = typeof str.identifier;
    namespace sym {
        const name: name;
        const identifier: unique symbol;
        type name = "symbol";
        type identifier = typeof identifier;
        type refersTo = symbol;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type sym = typeof sym.identifier;
    namespace und {
        const name: name;
        const identifier: unique symbol;
        type name = "undefined";
        type identifier = typeof identifier;
        type refersTo = undefined | null | never;
        type swapper<Value extends name | refersTo> = SetupSwapper<Value, name, refersTo>;
    }
    type und = typeof und.identifier;
}
export {};
//# sourceMappingURL=InterfaceParameters.d.ts.map