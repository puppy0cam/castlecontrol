/// <reference types="node" />
import { BattlesPerDay } from "./BattlesPerDay";
export default function offset_every_battle<T extends [...any[]]>(callback: (...args: Partial<T>) => void, offset?: number, ...args: T): BattlesPerDay<ReturnType<typeof setTimeout>>;
//# sourceMappingURL=offset_every_battle.d.ts.map