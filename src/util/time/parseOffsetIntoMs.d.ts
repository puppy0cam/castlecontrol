/**
 * parses an offset of date/time into a Date that can be used.
 *
 * format:
 * 1d5h21s
 *
 * Supports:
 *
 * >Days: d
 *
 * >Hours: h
 *
 * >Minutes: m
 *
 * >Seconds: s
 * @param info the string to parse
 */
export default function parseOffsetIntoMs(info: string, relativeTo?: number): number;
//# sourceMappingURL=parseOffsetIntoMs.d.ts.map