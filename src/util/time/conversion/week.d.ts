export declare function toMillisecond(value: number): number;
export declare function toSecond(value: number): number;
export declare function toMinute(value: number): number;
export declare function toHour(value: number): number;
export declare function toBattle(value: number): number;
export declare function toDay(value: number): number;
export declare function toWeek(value: number): number;
//# sourceMappingURL=week.d.ts.map