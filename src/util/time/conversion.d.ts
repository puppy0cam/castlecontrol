declare const units: {
    millisecond: "time";
    second: "time";
    minute: "time";
    hour: "time";
    battle: "time";
    day: "time";
    week: "time";
};
export default function conversion<unit extends keyof typeof units>(from: unit, to: unit, value: number): number;
export {};
//# sourceMappingURL=conversion.d.ts.map