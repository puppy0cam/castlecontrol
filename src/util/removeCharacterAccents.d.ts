interface String {
    /**
     * converts accented characters to regular english characters.
     */
    removeCharacterAccents(): string;
}
//# sourceMappingURL=removeCharacterAccents.d.ts.map