export default function iterateThroughArray<T>(...data: T[]): IterableIterator<T>;
//# sourceMappingURL=iterateThroughArray.d.ts.map