interface String {
    /**
     * Strips all characters that cannot be typed on a standard QWERTY keyboard.
     * This does not account for the numpad shortcut that can type characters based on codepoint.
     *
     * `"😂Hello😂World😂!"` would become `"HelloWorld!"`
     */
    removeNonEnglishCharacters(): string;
}
//# sourceMappingURL=removeNonEnglishCharacters.d.ts.map